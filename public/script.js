function updatePrice() {
    let s = document.getElementsByName("types");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let flag=0;
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
      price = prices.prodTypes[priceIndex];
    }
    
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = (select.value == "2" ? "block" : "none");
    
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radio) {
      if (radio.checked) {
        let optionPrice = prices.prodOptions[radio.value];
        if (optionPrice !== undefined) {
          price += optionPrice;
        }
      }
    });

    let quan = parseInt(document.getElementById("quantity").value);
    if(quan!==undefined)
    {
        if(quan>=0)
        {
            price*=quan;
        }
        else
        {
            flag=1;
        }
    }

    let checkDiv = document.getElementById("checkboxes");
    checkDiv.style.display = (select.value == "3" ? "block" : "none");
  
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
      if (checkbox.checked) {
        let propPrice = prices.prodProperties[checkbox.name];
        if (propPrice !== undefined) {
          price += propPrice;
        }
      }
    });
    let prodPrice = document.getElementById("Price");
    if(flag==0)
    {
        prodPrice.innerHTML = price + " рублей";
    }
    else
    {
        prodPrice.innerHTML="Введенно отрицательное количество товара";
    }
  }
  
  function getPrices() {
    return {
      prodTypes: [100, 200, 150],
      prodOptions: {
        option2: 10,
        option3: 15,
      },
      prodProperties: {
        prop1: 20,
        prop2: 30,
      }
    };
  }
  
  window.addEventListener('DOMContentLoaded', function (event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";
    
    let s = document.getElementsByName("types");
    let select = s[0];

    select.addEventListener("change", function(event) {
      let target = event.target;
      console.log(target.value);
      updatePrice();
    });
    let quan = document.getElementById("quantity");
    quan.addEventListener("change",function(event)
    {
        let q = event.target;
        console.log(q.value);
        updatePrice();
    });

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radio) {
      radio.addEventListener("change", function(event) {
        let r = event.target;
        console.log(r.value);
        updatePrice();
      });
    });
  
     
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
      checkbox.addEventListener("change", function(event) {
        let c = event.target;
        console.log(c.name);
        console.log(c.value);
        updatePrice();
      });
    });
  
    updatePrice();
  });
  